module.exports = {
  String: basicTypeCheck(String),
  Number: basicTypeCheck(Number),
  Boolean: basicTypeCheck(Boolean),
  BigInt: basicTypeCheck(BigInt),
  Object: basicTypeCheck(Object, value => !(value instanceof Array), 'an'),
  Array: (value, property, { propertyName }) => ({
    success: value === undefined || value instanceof Array,
    message: `${propertyName} must be an Array`
  })
}

const isArrayProperty = (aspects) => aspects instanceof Array && aspects.includes(Array)

function basicTypeCheck(type, additionalTest, article) {
  return typeCheck(type, value =>
    (value === undefined || value === null) || (value.constructor === type && (!additionalTest || additionalTest(value))),
  article)
}

function typeCheck(type, test, article = 'a') {
  return (value, property, { propertyName, aspects }) => isArrayProperty(aspects)
    ? {
        success: value === undefined || !(value instanceof Array) || value.every(test),
        message: `${propertyName} must be an array of ${type.name}s`,
        aspect: type.name
      }
    : {
        success: value === undefined || test(value),
        message: `${propertyName} must be ${article} ${type.name}`,
        aspect: type.name
      }
}
