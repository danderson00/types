const common = module.exports = {
  failuresFromResult: (repository, aspect, result, { property, parentProperty, failures = [] } = {}) => {
    if (result.success || result.hasOwnProperty('result') || result.hasOwnProperty('newValue')) {
      return failures
    }

    return [
      ...failures,
      ...(result.failures || []).map(x => ({
        ...x,
        property: common.fullPropertyName(property || x.property || result.property, parentProperty),
        aspect: result.aspect || repository.aspectName(aspect)
      })),
      ...((result.message || result.error)
        ? [{
            property: common.fullPropertyName(property || result.property, parentProperty),
            aspect: result.aspect || repository.aspectName(aspect),
            message: result.message || (result.error && result.error.message),
            ...(result.error && { error: result.error })
          }]
        : []
      )
    ]
  },
  fullPropertyName: (property, parentProperty) => parentProperty ? `${parentProperty}.${property}` : property
}
