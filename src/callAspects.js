const resolve = require('./callAspects.directives')
const { applyAsync, forceArray } = require('./utilities')

module.exports = repository => {
  return function applyCallAspects(callAspects, parameters, userContext, validate) {
    const mappedAspects = repository.mapCallAspects(callAspects)

    if(mappedAspects.length < 1 || typeof mappedAspects[mappedAspects.length - 1] !== 'function') {
      return { failures: [{ message: 'The last call aspect must be the target function' }] }
    }

    let target = mappedAspects[mappedAspects.length - 1]
    let failures = []
    let lastResult
    const aspects = mappedAspects.filter(x => x !== target)

    const resolveResult = result => applyAsync(result,
      result => ({ result: result || lastResult, failures })
    )

    if(aspects.length > 0) {
      return resolveResult(applyAspect(0, parameters, userContext))

      function applyAspect(index, parameters, context) {
        const next = (payload = {}) => {
          return index === aspects.length - 1
            ? lastResult = validate ? undefined : target(...(payload.parameters || parameters))
            : applyAspect(
                index + 1,
                payload.parameters || parameters,
                { ...context, ...payload.context }
              )
        }

        const fail = failure => {
          forceArray(failure).forEach(x =>
            failures.push(typeof x === 'string'
              ? { message: x }
              : x
            )
          )
        }

        const setTarget = targetFactory => { target = targetFactory(target, context) }

        return resolve(aspects[index])({ target, parameters, context, next, fail, setTarget })
      }
    } else {
      return resolveResult(validate ? undefined : target(parameters))
    }
  }
}
