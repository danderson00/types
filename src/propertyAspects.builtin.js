module.exports = {
  // constraints
  required: (value, property, { propertyName }) => ({
    success: value !== undefined && value !== null,
    message: `${propertyName} is required`
  }),
  min: minValue => function min(value, property, { propertyName }) {
    return {
      success: value === undefined || value >= minValue,
      message: `${propertyName} must be at least ${minValue}`
    }
  },
  max: maxValue => function max(value, property, { propertyName }) {
    return {
      success: value === undefined || value <= maxValue,
      message: `${propertyName} must be at most ${maxValue}`
    }
  },
  any: () => ({ success: true }), // for use with requireAll, better semantics than []

  // mutations
  defaultValue: defaultValue => value => ({ newValue: value === undefined ? defaultValue : value }),
  generated: generator => value => ({ newValue: generator(value) })
}