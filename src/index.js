const repository = require('./repository')
const typeAspects = require('./typeAspects')
const callAspects = require('./callAspects')
const { applyAsync } = require('./utilities')

module.exports = options => {
  const container = repository(options)
  const applyTypeAspects = typeAspects(container, options)
  const applyCallAspects = callAspects(container, options)

  const handleResult = result => applyAsync(result, result => {
    if(options.throwOnFailure) {
      if(result.failures.length > 0) {
        throw new Error(result.failures.map(x => x.message).join('. '))
      } else {
        return result.result
      }
    } else {
      return {
        ...result,
        success: result.failures.length === 0
      }
    }
  })

  return {
    ...container,
    apply: (type, target, context) => handleResult(
      applyTypeAspects(type || 'unknown type', target, { context })
    ),
    call: (target, parameters, context) => handleResult(
      applyCallAspects(target, parameters, context)
    ),
    validateCall: (target, parameters, context) => handleResult(
      applyCallAspects(target, parameters, context, true)
    )
  }
}
