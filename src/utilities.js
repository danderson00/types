const utilities = module.exports = {
  isPromise: target => (
    target instanceof Promise ||
    (target && typeof target.then === 'function' && typeof target.catch === 'function')
  ),
  applyAsync: (target, action) => utilities.isPromise(target) ? target.then(action) : action(target),
  forceArray: target => target instanceof Array ? target : [target],
  reverseLookup: (from, item) => Object.keys(from)[Object.values(from).indexOf(item)]
}