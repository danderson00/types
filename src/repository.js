const builtinTypeMaps = require('./typeMaps')
const builtinPropertyAspects = require('./propertyAspects.builtin')
const builtinTypeAspects = require('./typeAspects.builtin')
const { forceArray, reverseLookup } = require('./utilities')

const defaultOptions = {
  globalPropertyAspects: [],
  globalTypeAspects: [],
  globalCallAspects: []
}

module.exports = userOptions => {
  const options = { ...defaultOptions, ...userOptions }

  let typeAspects = { ...builtinTypeAspects, ...options.typeAspects }
  let propertyAspects = { ...builtinPropertyAspects, ...options.propertyAspects }
  let callAspects = { ...options.callAspects }
  let typeMaps = { ...builtinTypeMaps, ...options.typeMaps }

  const buildTypes = builder => builder({ ...typeAspects, ...propertyAspects })
  const buildCalls = builder => builder(callAspects)

  let types = typeof options.types === 'function'
    ? buildTypes(options.types)
    : { ...options.types }

  let calls = typeof options.calls === 'function'
    ? buildCalls(options.calls)
    : { ...options.calls }

  const mapId = (aspect, target, displayName = 'aspect') => {
    const notFound = message => ({
      notFound: () => ({ aspect: 'strict', failures: [{ message }] }),
      notFoundIgnored: () => ({ success: true })
    })[options.strict ? 'notFound' : 'notFoundIgnored']

    if(typeof aspect === 'string') {
      return target[aspect] || notFound(`No ${displayName} with id ${aspect} registered`)
    }
    return aspect || notFound(`No ${displayName} provided (was ${aspect})`)
  }

  const api = {
    addType: (id, type) => types = { ...types, [id]: type },
    addTypeAspect: (id, aspect) => typeAspects = { ...typeAspects, [id]: aspect },
    addPropertyAspect: (id, aspect) => propertyAspects = { ...propertyAspects, [id]: aspect },
    addCall: (id, aspects) => calls = { ...calls, [id]: aspects },
    addCallAspect: (id, aspect) => callAspects = { ...callAspects, [id]: aspect },
    addTypeMap: (constructor, aspect) => typeMaps = { ...typeMaps, [constructor.name]: aspect },

    typeCount: () => Object.keys(types).length,
    callCount: () => Object.keys(calls).length,

    mapPropertyAspects: aspects => [
      ...options.globalPropertyAspects,
      ...forceArray(aspects)
    ].map(
      aspect => mapId(typeMaps[aspect && aspect.name] || aspect, propertyAspects, 'property aspect')
    ),

    mapTypeAspects: aspects => {
      const type = mapId(aspects, types, 'type')
      return type.name === 'notFound'
        ? [type]
        : [
            ...options.globalTypeAspects,
            ...forceArray(type),
            ...(options.strict ? ['strict'] : [])
          ].map(
            aspect => mapId(aspect, { ...types, ...typeAspects }, 'type or aspect')
          )
    },

    mapCallAspects: callAspects => {
      const aspects = forceArray(
        typeof callAspects === 'string'
          ? mapId(callAspects, calls, 'calls')
          : callAspects
      )

      // a bit hacky - insert global call aspects before the target function
      return [
        ...aspects.slice(0, aspects.length - 1),
        ...options.globalCallAspects,
        aspects[aspects.length - 1]
      ].map(aspect => mapId(aspect, callAspects), 'call aspect')
    },

    aspectName: aspect => (
      reverseLookup(typeAspects, aspect) ||
      reverseLookup(propertyAspects, aspect) ||
      aspect.name ||
      'anonymous'
    ),

    buildTypes: builder => {
      const types = buildTypes(builder)
      Object.keys(types).forEach(id => api.addType(id, types[id]))
      return types
    },

    buildCalls: builder => {
      const calls = buildCalls(builder)
      Object.keys(calls).forEach(id => api.addCall(id, calls[id]))
      return calls
    }
  }

  return api
}
