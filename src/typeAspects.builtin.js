const { fullPropertyName } = require('./common')

module.exports = {
  trim: (target, { aspects }) => ({ result: extractPropertyNames(aspects).reduce(
    (result, property) => ({ ...result, [property]: target[property] }),
    {}
  )}),
  strict: (target, { aspects, parentProperty }) => {
    const propertyNames = extractPropertyNames(aspects)
    const failures = Object.keys(target || {}).reduce(
      (failures, property) => [...failures, ...(
        propertyNames.includes(property) ? [] : [{
          message: `${fullPropertyName(property, parentProperty)} is an invalid property name`,
          property: fullPropertyName(property, parentProperty)
        }]
      )],
      []
    )
    return { success: failures.length === 0, failures }
  },
  requireAll: (target, { aspects, parentProperty }) => (target
    ? extractPropertyNames(aspects).reduce(
        (result, property) => {
          const failures = [...result.failures, ...(
            target.hasOwnProperty(property) && target[property] !== undefined ? [] : [{
              message: `${fullPropertyName(property, parentProperty)} is required`,
              property: fullPropertyName(property, parentProperty)
            }]
          )]
          return { success: failures.length === 0, failures }
        },
        { success: true, failures: [] }
      )
    : {
        success: false,
        message: `${parentProperty ? `${parentProperty} must be an object` : `Object required`} with (${extractPropertyNames(aspects).join(', ')}) properties`,
        property: parentProperty
      }
  )
}

const extractPropertyNames = aspects => aspects.reduce(
  (properties, aspect) => (typeof aspect === 'object' && ! (aspect instanceof Array))
    ? [...properties, ...Object.keys(aspect)]
    : properties,
  []
)