const propertyAspects = require('./propertyAspects')
const { failuresFromResult, fullPropertyName } = require('./common')

module.exports = repository => {
  const applyPropertyAspects = propertyAspects(repository)

  return function applyTypeAspects(type, initialTarget, { context, parentProperty } = {}) {
    const mappedAspects = repository.mapTypeAspects(type)

    return mappedAspects.reduce(
      (result, typeAspect) => mergeResult(result, typeof typeAspect === 'function'
        ? applyTypeAspect(result.result, typeAspect, mappedAspects)
        : applyObjectDefinition(result.result, typeAspect)
      ),
      { failures: [], result: initialTarget }
    )

    function applyChildTypeAspects(initialTarget, property, aspects) {
      const typeAspectResult = applyTypeAspects(
        aspects,
        initialTarget && initialTarget[property],
        { context, parentProperty: fullPropertyName(property, parentProperty) }
      )

      return {
        result: {
          ...initialTarget,
          [property]: typeAspectResult.result
        },
        failures: (typeAspectResult.failures || [])
      }
    }

    function applyTypeAspect(target, aspect, aspects) {
      let aspectResult
      try {
        const aspectContext = { aspects, context, parentProperty }
        aspectResult = aspect(target, aspectContext)
      } catch (error) {
        aspectResult = { error, message: error.message }
      }

      return {
        failures: failuresFromResult(repository, aspect, aspectResult),
        result: aspectResult.result || target
      }
    }

    function applyObjectDefinition(initialTarget, definition) {
      return Object.keys(definition).reduce(
        (result, property) => mergeResult(result, isTypeDefinition(definition[property])
          ? applyChildTypeAspects(initialTarget, property, definition[property])
          : applyPropertyAspects(definition[property], result.result, property, { context, parentProperty })
        ),
        { failures: [], result: initialTarget }
      )
    }


    function isTypeDefinition(definition) {
      const definitions = definition instanceof Array ? definition : [definition]
      return typeof definition === 'string' || definitions.some(x => typeof x === 'object' && !(x instanceof Array))
    }

    function mergeResult(existing, incoming) {
      return {
        failures: [...existing.failures, ...incoming.failures],
        result: incoming.result
      }
    }
  }
}
