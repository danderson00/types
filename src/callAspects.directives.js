module.exports = aspect => (
  typeof aspect === 'function'
    ? aspect
    : ({ next, parameters, context = {}, fail, setTarget }) => {
        const newParameters = aspect.parameters ? aspect.parameters(...parameters) : parameters
        const newContext = { ...context, ...(aspect.context && aspect.context({ parameters: newParameters, context })) }

        if(aspect.validate) {
          const validateResult = aspect.validate({ parameters: newParameters, context: newContext })
          if(validateResult !== undefined && validateResult !== true) {
            fail(validateResult)
            return
          }
        }

        if(aspect.target) {
          setTarget(aspect.target)
        }

        const payload = {
          parameters: newParameters,
          context: newContext
        }

        const result = next(payload)

        return aspect.result ? aspect.result(result) : result
      }
)