const { failuresFromResult, fullPropertyName } = require('./common')

module.exports = repository => {
  return function applyPropertyAspects(aspects, initialTarget, property, { context, parentProperty } = {}) {
    return repository.mapPropertyAspects(aspects).reduce(
      ({ success, failures, result }, aspect) => {
        const aspectResult = applyPropertyAspect(result, property, aspect, aspects)

        return {
          failures: failuresFromResult(repository, aspect, aspectResult, { failures, property, parentProperty }),
          result: targetFromPropertyAspectResult(result, property, aspectResult)
        }
      },
      { failures: [], result: initialTarget }
    )

    function applyPropertyAspect(target, property, aspect, aspects) {
      const aspectContext = {
        property,
        target,
        context,
        aspects,
        propertyName: fullPropertyName(property, parentProperty)
      }

      try {
        return aspect(target && target[property], property, aspectContext)
      } catch (error) {
        return { error, message: error.message }
      }
    }

    function targetFromPropertyAspectResult(existing, property, result) {
      if (result.result) {
        return result.result

      } else if (result.newValue) {
        return {
          ...existing,
          [property]: result.newValue
        }
      }

      return existing
    }
  }
}
