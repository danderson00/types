const setup = require('./setup').types
const { trim, strict, requireAll } = require('../src/typeAspects.builtin')

test("trim removes undeclared properties", () => {
  const { validate } = setup([trim, { p1: [] }, { p2: [] }])
  validate({ p1: 1, p2: 2, p3: 3 }, { result: { p1: 1, p2: 2 } })
})

test("strict fails if any additional properties are present", () => {
  const { validate } = setup([strict, { p1: [], p2: [] }])
  validate({ p1: 1, p2: 2 })
  validate({ p1: 1, p2: 2, p3: 3 }, { failures: [{ aspect: 'strict', message: 'p3 is an invalid property name', property: 'p3' }] })
  validate({ p1: 1, p2: 2, p3: 3, p4: 4 }, { failures: [
    { aspect: 'strict', message: 'p3 is an invalid property name', property: 'p3' },
    { aspect: 'strict', message: 'p4 is an invalid property name', property: 'p4' }
  ] })
})

test("requireAll makes all properties required", () => {
  const { validate } = setup([requireAll, { p1: [], p2: [] }])
  validate({ p1: 1, p2: 2 })
  validate({ p2: 2 }, { failures: [{ aspect: 'requireAll', message: 'p1 is required' }] })
  validate({ p1: 1 }, { failures: [{ aspect: 'requireAll', message: 'p2 is required' }] })
  validate({}, { failures: [
      { aspect: 'requireAll', message: 'p1 is required' },
      { aspect: 'requireAll', message: 'p2 is required' }
  ] })
})

test("requireAll returns correct property name when nested", () => {
  const { validate } = setup({ p1: [requireAll, { p2: [] }] })
  validate({ p1: { p2: 2 } })
  validate({}, { failures: [{ aspect: 'requireAll', property: 'p1', message: 'p1 must be an object with (p2) properties' }] })
  validate({ p1: {} }, { failures: [{ aspect: 'requireAll', property: 'p1.p2', message: 'p1.p2 is required' }] })
})

