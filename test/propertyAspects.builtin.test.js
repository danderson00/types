const setup = require('./setup').types
const { required, min, max, defaultValue, generated } = require('../src/propertyAspects.builtin')

test("required fails if property is not present", () => {
  const { validate } = setup([{ p1: required, p2: [] }])
  validate({ p1: 1 })
  validate({}, { failures: [{ property: 'p1', aspect: 'required', message: 'p1 is required' }] })
})

test("min fails if value is present and below the specified value", () => {
  const { validate } = setup([{ p1: min(2) }])
  validate({})
  validate({ p1: 2 })
  validate({ p1: 1 }, { failures: [{ property: 'p1', aspect: 'min', message: 'p1 must be at least 2' }] })
})

test("max fails if value is present and above the specified value", () => {
  const { validate } = setup([{ p1: max(2) }])
  validate({})
  validate({ p1: 1 })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'max', message: 'p1 must be at most 2' }] })
})

test("defaultValue sets value if none is specified", () => {
  const { validate } = setup([{ p1: defaultValue(2) }])
  validate({}, { result: { p1: 2 } })
  validate({ p1: 2 })
})

test("generated sets property to value received by generator", () => {
  const { validate } = setup([{ p1: generated(x => x * 2) }])
  validate({ p1: 2 }, { result: { p1: 4 } })
})