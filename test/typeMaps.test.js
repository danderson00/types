const setup = require('./setup').types

test("applies string type checks", () => {
  const { validate } = setup({ p1: String })
  validate({})
  validate({ p1: 'test' })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'String', message: 'p1 must be a String' }] })
})

test("applies number type checks", () => {
  const { validate } = setup({ p1: Number })
  validate({})
  validate({ p1: 3 })
  validate({ p1: 'test' }, { failures: [{ property: 'p1', aspect: 'Number', message: 'p1 must be a Number' }] })
})

test("applies boolean type checks", () => {
  const { validate } = setup({ p1: Boolean })
  validate({})
  validate({ p1: true })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'Boolean', message: 'p1 must be a Boolean' }] })
})

test("applies bigint type checks", () => {
  const { validate } = setup({ p1: BigInt })
  validate({})
  validate({ p1: BigInt(1) })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'BigInt', message: 'p1 must be a BigInt' }] })
})

test("applies object type checks", () => {
  const { validate } = setup({ p1: Object })
  validate({})
  validate({ p1: {} })
  validate({ p1: [] }, { failures: [{ property: 'p1', aspect: 'Object', message: 'p1 must be an Object' }] })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'Object', message: 'p1 must be an Object' }] })
})

test("applies array type checks", () => {
  const { validate } = setup({ p1: Array })
  validate({})
  validate({ p1: [] })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'Array', message: 'p1 must be an Array' }] })
})

test("applies typed array type checks", () => {
  const { validate } = setup({ p1: [Array, Number] })
  validate({})
  validate({ p1: [] })
  validate({ p1: [1, 2] })
  validate({ p1: ['test'] }, { failures: [{ property: 'p1', aspect: 'Number', message: 'p1 must be an array of Numbers' }] })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'Array', message: 'p1 must be an Array' }] })
})

test("custom type maps", () => {
  function CustomType(value) {
    this.value = value
  }

  const { validate } = setup({ p1: CustomType }, { options: { typeMaps: { CustomType: target => ({
    success: target.value === 1,
    message: 'Must have value 1'
  }) } } })

  validate({ p1: new CustomType(1) })
  validate({ p1: new CustomType(2) }, { failures: [{ property: 'p1', aspect: 'CustomType', message: 'Must have value 1' }] })
})

test("handles null values", () => {
  const { validate } = setup({ p1: String, p2: Object })
  validate({ p1: null, p2: null })
})