const setup = require('./setup').types

test("applies simple type validations", () => {
  const oneProperty = target => ({
    success: Object.keys(target).length === 1,
    message: `Target must have one property`
  })
  const { validate } = setup(oneProperty)
  validate({ p1: 2 })
  validate({ p1: 2, p2: 3 }, { failures: [{ aspect: 'oneProperty', message: 'Target must have one property' }] })
})

test("applies simple type mutations", () => {
  const addUserId = target => ({ result: { ...target, userId: 'test' } })
  const { validate } = setup(addUserId)
  validate({ p1: 2 }, { p1: 2, userId: 'test' })
})

test("applies multiple type mutations in order", () => {
  const addUserId = target => ({ result: { ...target, userId: 'test' } })
  const annotateUserId = target => ({ result: { ...target, userId: target.userId + 'ing' } })
  const { validate } = setup([addUserId, annotateUserId])
  validate({ p1: 2 }, { result: { p1: 2, userId: 'testing' } })
})

test("passes all type aspects in context", () => {
  const aspect1 = { }
  const aspect2 = { }
  const assertContext = (target, context) => {
    expect(context.aspects).toEqual([aspect1, aspect2, assertContext])
    return { success: true }
  }
  const { validate } = setup([aspect1, aspect2, assertContext])
  validate({ p1: 1 })
})

test("object definitions can be recursive", () => {
  const equalsOne = value => ({
    success: value === 1,
    message: `Must equal 1`
  })
  const { validate } = setup({ p1: { p2: equalsOne } })
  validate({ p1: { p2: 1 } })
  validate({ p1: { p2: 'test' } }, { failures: [
    { property: 'p1.p2', aspect: 'equalsOne', message: 'Must equal 1' }
  ] })
})

test("context is passed to type aspects", () => {
  const validateContext = (_, { context }) => ({
    success: context === 'test',
    message: 'Invalid context'
  })
  const { validate } = setup(validateContext)
  validate({}, { context: 'test' })
  validate({}, { failures: [{ aspect: 'validateContext', message: 'Invalid context' }] })
})

test("typeAspects can return multiple decorated failures", () => {
  const fail = () => ({ failures: [{ message: 'test' }, { message: 'test2' }] })
  const { validate } = setup(fail)
  validate({}, { failures: [
      { aspect: 'fail', message: 'test' },
      { aspect: 'fail', message: 'test2' }
    ]})
})

test("type aspects can be specified by id", () => {
  const addProperty = value => ({ result: { ...value, test: 'test' } })
  const { validate } = setup(['addProperty', { p1: [] }], { options: {
      typeAspects: { addProperty }
    } })
  validate({}, { result: { test: 'test' } })
})

test("types can be specified by id", () => {
  const addProperty = value => ({ result: { ...value, test: 'test' } })
  const { validate } = setup({ p1: 'child' }, { options: {
      typeAspects: { addProperty },
      types: { child: addProperty }
    } })
  validate({}, { result: { p1: { test: 'test' } } })
})

test("type ids can be used as type aspect ids", () => {
  const addProperty = value => ({ result: { ...value, test: 'test' } })
  const { validate } = setup(['addProperty', { p1: [] }], { options: {
    types: { addProperty }
  } })
  validate({}, { result: { test: 'test' } })
})

test("globalTypeAspects option applies specified aspect to all types", () => {
  const addProperty = value => ({ result: { ...value, test: 'test' } })
  const { validate } = setup({ p1: { p2: [] } }, { options: {
    globalTypeAspects: [addProperty]
  } })
  validate({}, { result: { p1: { test: 'test' }, test: 'test' } })
  validate({ p1: { p2: 2 } }, { result: { p1: { p2: 2, test: 'test' }, test: 'test' } })
})

test("globalTypeAspects option applies specified aspect id to all types", () => {
  const addProperty = value => ({ result: { ...value, test: 'test' } })
  const { validate } = setup({ p1: { p2: [] } }, { options: {
    typeAspects: { addProperty },
    globalTypeAspects: ['addProperty']
  } })
  validate({}, { result: { p1: { test: 'test' }, test: 'test' } })
  validate({ p1: { p2: 2 } }, { result: { p1: { p2: 2, test: 'test' }, test: 'test' } })
})
