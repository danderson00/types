const setup = require('./setup').types

test("applies simple property validations", () => {
  const isEven = (value, property) => ({
    success: value % 2 === 0,
    message: `${property} must be even`
  })
  const { validate } = setup({ p1: isEven })
  validate({ p1: 2 })
  validate({ p1: 3 }, { failures: [{ property: 'p1', aspect: 'isEven', message: 'p1 must be even' }] })
})

test("applies simple property mutations by returning new value", () => {
  const addOne = value => ({ newValue: value + 1 })
  const { validate } = setup({ p1: addOne })
  validate({ p1: 1 }, { result: { p1: 2 } })
})

test("applies simple property mutations by returning new result", () => {
  const addOne = (value, property, { target }) => ({
    result: { ...target, [property]: value + 1 }
  })
  const { validate } = setup({ p1: addOne })
  validate({ p1: 1 }, { result: { p1: 2 } })
})

test("applies multiple mutations in order", () => {
  const cast = value => ({ newValue: value.toString() })
  const addOne = value => ({ newValue: value + 1 })
  const { validate } = setup({ p1: [cast, addOne] })
  validate({ p1: 1 }, { result: { p1: '11' } })
})

test("exceptions result in failures", () => {
  const error = new Error('error!')
  const throwError = () => { throw error }
  const { validate } = setup({ p1: throwError })
  validate({ p1: 1 }, { failures: [{ property: 'p1', aspect: 'throwError', message: 'error!', error }] })
})

test("allows multiple type definitions", () => {
  const cast = value => ({ newValue: value.toString() })
  const addOne = value => ({ newValue: value + 1 })
  const { validate } = setup([{ p1: cast }, { p1: addOne }])
  validate({ p1: 1 }, { result: { p1: '11' } })
})

test("context is passed to property aspects", () => {
  const validateContext = (target, property, { context }) => ({
    success: context === 'test',
    message: 'Invalid context'
  })
  const { validate } = setup({ p1: validateContext })
  validate({}, { context: 'test' })
  validate({}, { failures: [{ property: 'p1', aspect: 'validateContext', message: 'Invalid context' }] })
})

test("all property aspects are passed to context", () => {
  const validateContext = (target, property, { aspects }) => {
    expect(aspects).toEqual([validateContext, Number])
    return { success: true }
  }
  const { validate } = setup({ p1: [validateContext, Number] })
  validate({}, { context: 'test' })
})

test("property aspects can return multiple decorated failures", () => {
  const fail = () => ({ failures: [{ message: 'test' }, { message: 'test2' }] })
  const { validate } = setup({ p1: [fail] })
  validate({}, { failures: [
    { property: 'p1', aspect: 'fail', message: 'test' },
    { property: 'p1', aspect: 'fail', message: 'test2' }
  ]})
})

test("failures returned from child properties are decorated correctly", () => {
  const fail = () => ({ failures: [{ message: 'test' }, { message: 'test2' }] })
  const { validate } = setup({ p1: { p2: { p3: [fail] } } })
  validate({}, { failures: [
      { property: 'p1.p2.p3', aspect: 'fail', message: 'test' },
      { property: 'p1.p2.p3', aspect: 'fail', message: 'test2' }
    ]})
})

test("specifying a single string for a property aspect refers to a type id", () => {
  const { validate } = setup({ p1: 'child' }, { options: {
      types: { child: { value: Number } }
    } })
  validate({ p1: { value: 'test' } }, { failures: [{ property: 'p1.value', aspect: 'Number' }] })
})

test("property aspects can be specified by id", () => {
  const addOne = value => ({ newValue: value + 1 })
  const { validate } = setup({ p1: ['addOne'] }, { options: {
      propertyAspects: { addOne }
    } })
  validate({ p1: 1 }, { result: { p1: 2 } })
})

test("globalPropertyAspects option applies specified aspect to all properties", () => {
  const addOne = value => ({ newValue: value + 1 })
  const { validate } = setup({ p1: [], p2: [] }, { options: {
    globalPropertyAspects: [addOne]
  } })
  validate({ p1: 1, p2: 2 }, { result: { p1: 2, p2: 3 } })
})

test("globalPropertyAspects option applies specified aspect ids to all properties", () => {
  const addOne = value => ({ newValue: value + 1 })
  const { validate } = setup({ p1: [], p2: [] }, { options: {
    propertyAspects: { addOne },
    globalPropertyAspects: ['addOne']
  } })
  validate({ p1: 1, p2: 2 }, { result: { p1: 2, p2: 3 } })
})