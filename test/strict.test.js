const setup = require('./setup').types

describe("off", () => {
  test("missing types are ignored", () => {
    const { validate } = setup('missing')
    validate({})
  })

  test("undefined types are ignored", () => {
    const { validate } = setup(undefined)
    validate({})
  })

  test("missing type aspects are ignored", () => {
    const { validate } = setup(['missing'])
    validate({})
  })

  test("missing property aspects are ignored", () => {
    const { validate } = setup({ p1: 'missing' })
    validate({})
  })

  test("undefined property aspects are ignored", () => {
    const { validate } = setup({ p1: undefined })
    validate({})
  })
})

describe("on", () => {
  test("missing types fails validation", () => {
    const { validate } = setup('missing', { options: { strict: true } })
    validate({ id: 1 }, { failures: [{ aspect: 'strict', message: 'No type with id missing registered' }] })
  })

  test("undefined types fails validation", () => {
    const { validate } = setup(undefined, { options: { strict: true } })
    validate({ id: 1 }, { failures: [{ aspect: 'strict', message: 'No type provided (was undefined)' }] })
  })

  test("missing child types fails validation", () => {
    const { validate } = setup({ p1: 'missing' }, { options: { strict: true } })
    validate({ p1: { id: 1 } }, { failures: [{ aspect: 'strict', message: 'No type with id missing registered' }] })
  })

  test("missing type aspects fails validation", () => {
    const { validate } = setup(['missing'], { options: { strict: true } })
    validate({}, { failures: [{ aspect: 'strict', message: 'No type or aspect with id missing registered' }] })
  })

  test("undefined type aspects fails validation", () => {
    const { validate } = setup([undefined], { options: { strict: true } })
    validate({}, { failures: [{ aspect: 'strict', message: 'No type or aspect provided (was undefined)' }] })
  })

  test("missing property aspects fails validation", () => {
    const { validate } = setup({ p1: ['missing'] }, { options: { strict: true } })
    validate({}, { failures: [{ aspect: 'strict', message: 'No property aspect with id missing registered' }] })
  })

  test("undefined property aspects fails validation", () => {
    const { validate } = setup({ p1: undefined }, { options: { strict: true } })
    validate({}, { failures: [{ aspect: 'strict', message: 'No property aspect provided (was undefined)' }] })
  })

  test("strict type aspect is added to all types", () => {
    const { validate } = setup({ p1: Number, p2: { p3: String } }, { options: { strict: true } })
    validate({})
    validate({ p1: 1, p2: { p3: 'test' } })
    validate({ p3: 1 }, { failures: [{ aspect: 'strict', property: 'p3', message: 'p3 is an invalid property name' }] })
    validate({ p1: 1, p2: { p4: 'test' } }, { failures: [{ aspect: 'strict', property: 'p2.p4', message: 'p2.p4 is an invalid property name' }] })
  })
})
