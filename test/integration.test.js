const types = require('..')

test("nested structure with strict mode, globals, composition and custom aspects", () => {
  const validator = types({
    strict: true,
    types: {
      timestamped: {
        timestamp: ['timestamp']
      }
    },
    typeAspects: {
      appendUserId: {
        userId: (value, property, { context }) => ({ newValue: context.userId })
      }
    },
    propertyAspects: {
      timestamp: () => ({ newValue: 1 }),
      profanityFilter: value => ({
        success: !value || !value.toString().includes('shit'),
        message: 'No profanity allowed!'
      })
    },
    globalPropertyAspects: ['profanityFilter']
  })

  validator.buildTypes(({ required, requireAll, any, appendUserId }) => ({
    product: [appendUserId, {
      id: [String, required],
      name: String
    }],
    order: ['timestamped', {
      productId: String,
      quantity: Number,
      notes: any
    }, requireAll]
  }))

  expect(validator.apply('product', {
    id: '1',
    name: 'one'
  }, { userId: 'user' })).toEqual({
    success: true,
    failures: [],
    result: {
      id: '1',
      name: 'one',
      userId: 'user'
    }
  })

  expect(validator.apply('product', {
    name: 7
  }, { userId: 'user' })).toEqual({
    success: false,
    failures: [
      { aspect: 'required', property: 'id', message: 'id is required' },
      { aspect: 'String', property: 'name', message: 'name must be a String' }
    ],
    result: {
      name: 7,
      userId: 'user'
    }
  })

  expect(validator.apply('order', {
    productId: '1',
    quantity: 1,
    notes: ''
  })).toEqual({
    success: true,
    failures: [],
    result: {
      productId: '1',
      quantity: 1,
      notes: '',
      timestamp: 1
    }
  })

  expect(validator.apply('order', {
    quantity: '1',
    notes: ''
  })).toEqual({
    success: false,
    failures: [
      { aspect: 'Number', property: 'quantity', message: 'quantity must be a Number' },
      { aspect: 'requireAll', property: 'productId', message: 'productId is required' }
    ],
    result: {
      quantity: '1',
      notes: '',
      timestamp: 1
    }
  })
})

test("type and call counts", () => {
  const validator = types({
    types: () => ({ type1: {}, type2: [{}] }),
    calls: { call1: () => {}, call2: [() => {}] }
  })
  validator.addType('type3', {})
  validator.addType('type4', [{}])
  validator.addCall('call3', () => {})
  expect(validator.typeCount()).toBe(4)
  expect(validator.callCount()).toBe(3)
})