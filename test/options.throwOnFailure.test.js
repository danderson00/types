const types = require('..')

describe("types", () => {
  const validate = types({ throwOnFailure: true, types: {
    test: {
      p1: Number,
      p2: String
    }
  }})

  test("returns result on success", () => {
    expect(validate.apply('test', { p1: 1, p2: '2' })).toEqual({ p1: 1, p2: '2' })
  })

  test("throws on failure", () => {
    expect(() => validate.apply('test', { p1: '1', p2: 2 })).toThrow('p1 must be a Number. p2 must be a String')
  })
})

describe("synchronous calls", () => {
  const validate = types({ throwOnFailure: true, calls: {
    test: [
      { validate: ({ parameters: [x] }) => x.length > 3 || 'Parameter must be more than three characters' },
      x => x
    ],
    asyncTest: [
      async ({ parameters: [x], next, fail }) => {
        if(x.length < 4) {
          fail('Parameter must be more than three characters')
        }
        const result = await next()
        if(result.length < 4) {
          fail('Result must be more than three characters')
        }
        return result
      },
      async x => x
    ]
  }})

  test("returns result on success", () => {
    expect(validate.call('test', ['Hello'])).toBe('Hello')
  })

  test("throws on failure", () => {
    expect(() => validate.call('test', ['Hel'])).toThrow('Parameter must be more than three characters')
  })

  test("returns async result on success", async () => {
    expect(await validate.call('asyncTest', ['Hello'])).toBe('Hello')
  })

  test("returns rejected promise on failure", async () => {
    expect(validate.call('asyncTest', ['Hel'])).rejects.toMatchObject({ message: 'Parameter must be more than three characters. Result must be more than three characters' })
  })
})
