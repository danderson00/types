const setup = require('./setup').calls

test("result", () => {
  const { validate } = setup([
    { result: x => x + ' world!' },
    x => x
  ])
  validate(['Hello'], { result: 'Hello world!' })
})

test("parameters", () => {
  const { validate } = setup([
    { parameters: (x, y) => [x + ' there', ' crazy ' + y] },
    (x, y) => x + y
  ])
  validate(['Hello', 'world!'], { result: 'Hello there crazy world!' })
})

test("target", () => {
  const { validate } = setup([
    { target: target => x => target(x) + ' world!' },
    x => x
  ])
  validate(['Hello'], { result: 'Hello world!' })
})

test("validate", () => {
  const { validate } = setup([
    { validate: ({ parameters: [x] }) => x.length > 3 || 'Parameter must be more than three characters' },
    x => x
  ])
  validate(['Hel'], { failures: [{ message: 'Parameter must be more than three characters' }] })
  validate(['Hello'], { result: 'Hello' })
})

test("context", () => {
  const { validate } = setup([
    { context: () => ({ p2: 2 }) },
    { context: ({ context }) => { expect(context).toEqual({ p1: 1, p2: 2 }) } },
    () => {}
  ])
  validate([], { context: { p1: 1 } })
})

test("multiple", () => {
  const { validate } = setup([
    { result: x => x + '!' },
    { parameters: (x, y) => [x + ' there', ' crazy ' + y] },
    { target: target => (x, y) => target(x) + y },
    x => x.toUpperCase()
  ])
  validate(['Hello', 'world'], { result: 'HELLO THERE crazy world!' })
})