const setup = require('./setup').calls

test("executes target function and encapsulates result", () => {
  const { validate } = setup(hello => hello + ' world!')
  validate(['Hello'], { result: 'Hello world!' })
})

test("executes target function with no aspects", () => {
  const { validate } = setup([hello => hello + ' world!'])
  validate(['Hello'], { result: 'Hello world!' })
})

test("returns result from aspect", () => {
  const result = {}
  const { validate } = setup([() => result, () => {}])
  validate([], { result })
})

test("returns result from aspect", () => {
  const result = {}
  const { validate } = setup([
    () => result,
    () => {}
  ])
  validate([], { result })
})

test("target does not execute if aspect does not call next", () => {
  const { validate } = setup([
    () => {},
    () => { throw new Error('should not execute') }
  ])
  validate([], { result: undefined })
})

test("calling next executes target", () => {
  const spy = jest.fn(x => x)
  const { validate } = setup([
    ({ next }) => next(),
    spy
  ])
  validate(['test'], { result: 'test' })
  expect(spy.mock.calls).toEqual([['test']])
})

test("calling next executes next aspect", () => {
  const spy = jest.fn()
  const { validate } = setup([
    ({ next }) => next(),
    spy,
    () => {}
  ])
  validate(['test'])
  expect(spy.mock.calls).toMatchObject([[{
    parameters: ['test']
  }]])
})

test("calling next with parameters replaces provided parameters", () => {
  const spy = jest.fn()
  const { validate } = setup([
    ({ next }) => next({ parameters: ['test2', 2] }),
    ({ next }) => next(),
    spy
  ])
  validate(['test', 1])
  expect(spy.mock.calls).toEqual([['test2', 2]])
})

test("context is passed to aspects", () => {
  const spy = jest.fn()
  const context = {}
  const { validate } = setup([
    spy,
    () => {}
  ])
  validate([1], { context })
  expect(spy.mock.calls).toMatchObject([[{ context }]])
})

test("context passed to next is additive", () => {
  const spy = jest.fn()
  const { validate } = setup([
    ({ next }) => next({ context: { p2: 2 } }),
    ({ next }) => next({ context: { p1: 2, p3: 3 } }),
    spy,
    () => {}
  ])
  validate([1], { context: { p1: 1 } })
  expect(spy.mock.calls).toMatchObject([[{ context: { p1: 2 , p2: 2, p3: 3 } }]])
})

test("failures from aspects accumulate", () => {
  const { validate } = setup([
    ({ fail, next }) => (fail('1'), next(), fail('4')),
    ({ fail }) => fail([{ message: '2' }, new Error('3')]),
    x => x
  ])
  validate([1], { failures: [{ message: '1'}, { message: '2' }, { message: '3' }, { message: '4' }] })
})

test("results are returned from async aspects", async () => {
  const result = {}
  const { validate } = setup([
    async () => result,
    () => {}
  ])
  await validate.async([], { result })
})

test("failures are returned from async aspects", async () => {
  const { validate } = setup([
    async ({ fail, next }) => (fail('1'), next(), fail('4')),
    async ({ fail }) => fail([{ message: '2' }, new Error('3')]),
    x => x
  ])
  await validate.async([1], { failures: [{ message: '1'}, { message: '2' }, { message: '3' }, { message: '4' }] })
})

test("results are returned from async targets", async () => {
  const { validate } = setup(async hello => hello + ' world!')
  await validate.async(['Hello'], { result: 'Hello world!' })
})

test("results are returned from async targets with no aspects", async () => {
  const { validate } = setup([async hello => hello + ' world!'])
  await validate.async(['Hello'], { result: 'Hello world!' })
})

test("results are flowed through aspects", async () => {
  const { validate } = setup([
    async ({ next }) => next(),
    ({ next }) => next(),
    x => new Promise(r => setTimeout(() => r(x)))
  ])
  await validate.async([1], { result: 1 })
})

test("exceptions from aspects are flowed", () => {
  const { validate } = setup([
    () => { throw new Error('test') },
    () => {}
  ])
  expect(() => validate([1])).toThrow({ message: 'test' })
})

test("exceptions from targets are flowed", () => {
  const { validate } = setup([
    ({ next }) => next(),
    () => { throw new Error('test') }
  ])
  expect(() => validate([1])).toThrow({ message: 'test' })
})

test("rejected promises from aspects are flowed", async () => {
  const { validate } = setup([
    async () => { throw new Error('test') },
    () => {}
  ])
  expect(validate.async([])).rejects.toMatchObject({ message: 'test' })
})

test("rejected promises from targets are flowed", async () => {
  const { validate } = setup([
    ({ next }) => next(),
    async () => { throw new Error('test') }
  ])
  expect(validate.async([])).rejects.toMatchObject({ message: 'test' })
})

test("global call aspects are applied after other aspects", () => {
  const { validate } = setup([
    ({ parameters: [x], next }) => next({ parameters: [x + '2'] }),
    x => x + '4'
  ], { options: { globalCallAspects: [
    ({ parameters: [x], next }) => next({ parameters: [x + '3'] })
  ]}})
  validate(['1'], { result: '1234' })
})

test("validate does not execute target function", () => {
  const spy = jest.fn()
  const { validate } = setup([
    ({ next }) => next(),
    spy
  ])
  validate(['test'], { validate: true, result: undefined })
  expect(spy.mock.calls.length).toBe(0)
})
