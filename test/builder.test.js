const types = require('..')

const expectedAspects = ['property', 'type', 'required', 'min', 'max', 'any', 'defaultValue', 'trim', 'requireAll']

test("builder is passed all aspects", () => {
  const validator = types()
  validator.addPropertyAspect('property', () => {})
  validator.addTypeAspect('type', () => {})
  validator.buildTypes(aspects => {
    expect(Object.keys(aspects)).toEqual(expect.arrayContaining(expectedAspects))
    return {}
  })
})

test("types option can be a builder", () => {
  types({
    propertyAspects: { property: () => {} },
    typeAspects: { type: () => {} },
    types: aspects => {
      expect(Object.keys(aspects)).toEqual(expect.arrayContaining(expectedAspects))
      return {}
    }
  })
})

test("calls option can be a builder", () => {
  types({
    callAspects: { call: () => {} },
    calls: aspects => {
      expect(Object.keys(aspects)).toEqual(['call'])
    }
  })
})