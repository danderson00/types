const typeAspects = require('../src/typeAspects')
const callAspects = require('../src/callAspects')
const repository = require('../src/repository')

module.exports = {
  types: (type, { typeMap, options } = {}) => {
    const container = repository(options)
    const applyTypeAspects = typeAspects(container, options)

    container.addType('type', type)
    if (typeMap) {
      container.addTypeMap(typeMap.type, typeMap.aspect)
    }

    const validate = (input, { result = input, failures = [], context } = {}) => (
      expect(applyTypeAspects(type, input, { context })).toMatchObject({ failures, result })
    )

    return { container, validate }
  },
  calls: (aspects, { options } = {}) => {
    const container = repository(options)
    container.addCall('call', aspects)
    const applyCallAspects = callAspects(container, options)

    const validate = (parameters, { result, failures = [], context, validate } = {}) => (
      expect(applyCallAspects('call', parameters, context, validate)).toMatchObject({ failures, result })
    )

    validate.async = async (parameters, { result, failures = [], context } = {}) => (
      expect(await applyCallAspects('call', parameters, context)).toMatchObject({ failures, result })
    )

    return { container, validate }
  }
}